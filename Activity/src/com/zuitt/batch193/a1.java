package com.zuitt.batch193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class a1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int in = 0;
        try {
            System.out.println("Input an integer whose factorial will be computed");
            in = input.nextInt();
        } catch(InputMismatchException e){
            System.out.println("Input is not a number");
        } catch(Exception e){ // Exception to catch any errors
            System.out.println("Invalid Input");
        } finally {
            int product = 1;
            int i = 1;
            if (in > 0) {
                while (i <= in) {
                    product *= i;
                    i++;
                }
                System.out.println("The factorial of " + in + " is " + product);
            } else if (in < 0) {
                for (int o = in; o < 0; o++) {
                    product *= o;
                }
                System.out.println("The factorial of " + in + " is " + product);
            } else {
                System.out.println("Zero is not accepted");
            }
        }
    }
}


